<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
//ruta http://localhost:81/api/v1/periodo 
//group pefix => api/v1 sirve para agregar el prefijo api/v1 a todas las rutas dentro del grupo.
$router->group(['prefix' => 'api/v1'], function () use ($router) {

    $router->get('/periodo', 'EcoSipController@periodo');
    $router->post('/alumnos-periodo', 'EcoSipController@alumnos_periodo');
});
