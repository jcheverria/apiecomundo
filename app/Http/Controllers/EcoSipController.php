<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EcoSipController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function periodo(Request $request)
    {
        $table = DB::select("SELECT Max(cod_param) as periodo from Parametros p where p.estado ='A'");
        return response()->json($table);
    }
    public function alumnos_periodo(Request $request)
    {
        try {
            $table = DB::select(DB::raw("exec sp_ecosip_datos_alumnos :cod_per"), [
                ':cod_per' => $request->periodo,
            ]);
            return response()->json($table, 200);
        } catch (\Throwable $th) {
            return response()->json($th, 500);
        }
    }
}
